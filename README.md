
This is the user guide on how to make use of the TDAQ continuous
integration facilities.

Quick Start
===========

Add a file called `.gitlab-ci.yml` to your top-level project directory.
See https://gitlab.cern.ch/help/ci/README.md for documentation on
this file, especially https://gitlab.cern.ch/help/ci/yaml/README.md

You can use this as your [initial template](https://gitlab.cern.ch/atlas-tdaq-software/tdaq_ci/-/blob/master/includes/tdaq-ci.yml):

```yaml
include: 'https://gitlab.cern.ch/atlas-tdaq-software/tdaq_ci/raw/master/includes/tdaq-ci.yml'
```

It will simply compile your package against the nightly release for a set of build
configurations.

### Customization 

If your project is in `tdaq-common` and not the main TDAQ project, you should add the 
following to the file:

```yaml
variables:
   project: tdaq-common
   RELEASE: tdaq-commmon-nightly
```

### More Customization

Check the gitlab help how the content of the include file is merged with your own declarations.

E.g. the template will only run CI jobs on the master branch. If you prefer to jobs on any
branch add this:

```yaml
.ci_build_base:
    only:
    - branches
```

If you would rather not have a CI job on every commit, but only if you submit a tag, add this:

```yaml
.ci_build_base:
    only:
    - tags
```

If you have a long-term branch that will be part of a special nightly build, change the 
content of `.gitlab-ci.yml` in that branch to e.g.

```yaml
variables:
    RELEASE: tdaq-99-00-07
```

Using the Docker images
=======================

We build both a `SLC 6` and a `CentOS 7` version of our build containers, as well as an 
experimental `Centos 8` version. You can use them
yourself via any means that seems best. The containers assume that external software
is available via `cvmfs`, so ideally that is available natively on your system.
The `latest` tag points to `centos7` at the moment.

A `tdaqsw` user is pre-defined in the container if you don't want to do things as root.

Fat containers can be built on top of these if desired.

You can the full list of available containers [here](https://gitlab.cern.ch/atlas-tdaq-software/tdaq_ci/container_registry)

Docker
------

Install `docker` on your machine, add yourself to the `docker` or `dockerroot` group.

```shell
docker run -it -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:centos7 bash
su - tdaqsw
% source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup tdaq-08-02-01
% mkdir work
% cd work
% cp /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/cmake/templates/CMakeLists.txt .
% kinit -f rhauser@CERN.CH
% getpkg hltsv
% cmake_config
% cd $CMTCONFIG
% make -j 4 install
```

Note: you can not easily run an X11 based application inside a docker container.

Singularity
-----------

Install the `singularity` or `singularity-container` (for Ubuntu) package on your system.

```shell
singularity pull docker:///cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:centos7
singularity shell -B /cvmfs:/cvmfs:ro tdaq_ci-centos7.simg
```

Note that `singularity` will run the container under your user ID, with your home directory mounted and
everything in the host's PID namespace. The good thing is that this will allow you to run X11 applications,
the bad thing that the separation is not very strong.

You can use the `-c` or `-C` options to have a stronger separation at the cost of not being able to run
X11 apps.

GitLab CI
----------

Specify the container in the `image:` tag of your job. Use the `cvmfs` tag in `tags:`.

```yaml
stages:
   - build

myjob:
    image: gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:centos7
    tags:
        - cvmfs
    script:
        - source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup tdaq-09-02-01
        - do_something_with_checked_out_code
```

Kubernetes
----------

See these [examples](https://:@gitlab.cern.ch:8443/atlas-tdaq-software/tests/tdaq-kube) on how to use CVMFS and TDAQ 
inside a [Kubernetes](https://kubernetes.io) cluster.

