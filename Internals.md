
This project builds the special images that are used
for TDAQ continuous integration tests. 

  - Two base images based on SLC6 and CentOS7 rsp. A user
    can specify these in the image: key and execute
    arbitrary commands on it. The images have all additional
    RPMs that are needed for development installed.

       image: gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:centos7
       image: gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:slc7

    The tags: key should include 'tdaq' to make sure the 
    test runs on a TDAQ specific runner.

  - Several images that have the current nightly and dev releeases
    installed (run-time only, no sources). They are identified
    by using either the `nightly` or `dev` name, followed
    by a valid CMTCONFIG tag, e.g.:

       image: gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci/nightly:x86_64-centos7-gcc8-opt
       image: gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci/dev:x86_64-centos7-gcc8-dbg

    These images might not have been properly produced or contain non-working releases
    as might happen for a nightly
