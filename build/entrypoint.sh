#!/bin/bash
# 
# $RELEASE = release to setup
# $CONFIG  = configuration to setup
# 
export PATH=/usr/sbin:/usr/bin:/sbin:/bin

if [ -f $CI_PROJECT_DIR/.done ]; then
   exit 0
else
   touch $CI_PROJECT_DIR/.done
fi

if [ -z "${RELEASE}" -o -z "${CONFIG}" ]; then
    echo "Both $RELEASE and $CONFIG have to be set"
    exit 1
fi

PACKAGE=$(basename $CI_PROJECT_DIR)
cd $CI_PROJECT_DIR || exit 1

mkdir -p .build/area/$CI_PROJECT_NAME
cp -r * .build/area/$CI_PROJECT_NAME/
cd .build/area

source /afs/cern.ch/atlas/project/tdaq/cmake/cmake_tdaq/bin/cm_setup.sh ${RELEASE} ${CONFIG}

case "${RELEASE}" in 
    tdaq-common-*)
        project=tdaq-common
        ;;
    dqm-common-*)
        project=dqm-common
        ;;
    *)
        project=tdaq
        ;;
esac

cat > CMakeLists.txt <<EOF
cmake_minimum_required(VERSION 3.6.0)
find_package(TDAQ)
include(CTest)
tdaq_work_area()
EOF

cat > CTestConfig.cmake <<EOF
set(CTEST_PROJECT_NAME "${project}")
set(CTEST_NIGHTLY_START_TIME "00:00:00 CEST")

set(CTEST_DROP_METHOD "http")
set(CTEST_DROP_SITE "atlas-tdaq-sw.web.cern.ch")
set(CTEST_DROP_LOCATION "/atlas-tdaq-sw/cdash/submit.php?project=${project}")
set(CTEST_DROP_SITE_CDASH TRUE)

set(CTEST_USE_LAUNCHERS 1)
EOF

mkdir build
cd build
cmake -DBUILDNAME=${PACKAGE}-${RELEASE}-${CONFIG} ..
make -j 4 -k ExperimentalStart ExperimentalConfigure ExperimentalBuild install/fast ExperimentalTest ExperimentalSubmit

