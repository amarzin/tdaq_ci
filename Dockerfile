FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
MAINTAINER Reiner.Hauser@cern.ch
RUN useradd tdaqsw -u 42056 && yum install -y epel-release && yum clean all
RUN /usr/bin/grep -q '^VERSION="9' /etc/os-release && (yum install -y gcc-c++ libstdc++-static expat-devel libaio-devel man-db && yum clean all) || /usr/bin/true
RUN yum install --skip-broken -y \
    /usr/bin/lsb_release \
    openssh-clients \
    git \
    rpm-build \
    libcurl-devel \
    alsa-lib \
    audit-libs \
    bzip2-libs \
    cracklib \
    cyrus-sasl-lib \
    device-mapper \
    e2fsprogs-libs \
    expat \
    fontconfig \
    freetype \
    gamin \
    gdbm \
    giflib \
    glib2 \
    glibc \
    glibc-devel \
    gmp \
    keyutils-libs \
    krb5-libs \
    libICE \
    libSM \
    libX11 \
    libXau \
    libXcursor \
    libXdamage \
    libXdmcp \
    libXext \
    libXfixes \
    libXft \
    libXi \
    libXinerama \
    libXmu \
    libXp  \
    libXpm \
    libXrandr \
    libXrender \
    libXt \
    libXtst \
    libXxf86vm \
    libaio \
    libdrm \
    libpciaccess \
    libgcc \
    libgfortran \
    libidn \
    libjpeg-turbo \
    libpng \
    libselinux \
    libsepol \
    libtiff \
    libuuid \
    libxcb \
    libxml2 \
    libxml2-devel \
    mesa-dri-drivers \
    mesa-dri-filesystem \
    mesa-libGL \
    mesa-libGLU \
    ncurses \
    ncurses-libs \
    nspr \
    nss \
    nss-softokn \
    nss-util \
    openldap \
    openldap-devel \
    motif-devel \
    openmotif \
    openssl \
    openssl-devel \
    pam \
    pam-devel \
    readline \
    readline-devel \
    sqlite \
    tk \
    tcl \
    zlib \
    freetype-devel \
    libXext-devel \
    libpng-devel \
    mesa-libGL-devel \
    mesa-libGLU-devel \
    ncurses-devel \
    libX11-devel \
    libXau-devel \
    libXdamage-devel \
    libXdmcp-devel \
    libXfixes-devel \
    libXxf86vm-devel \
    libdrm-devel \
    libxcb-devel \
    xorg-x11-proto-devel \
    xorg-x11-xbitmaps \
    libXaw-devel \
    libtool-ltdl \
    tcsh \
    zsh \
    procmail \
    autoconf \
    automake \
    libtool  \
    bzip2-devel \
    bind-utils \
    libuuid-devel \
    zlib-devel \
    libxslt \
    libXpm-devel \
    libXft-devel \
    krb5-devel \
    which \
    tar \
    krb5-workstation \
    xrootd-client \
    net-tools \
    iproute \
    atlas \
    gnutls-devel \
    cppcheck \
    rdma-core-devel \
    libtirpc-devel \
    libnsl2-devel \
    libnsl \
    lz4-devel \
    sudo \
    pcre2-devel \
    libzstd \
    libxkbcommon-devel \
    libxkbcommon-x11-devel \
    xcb-util-devel \
    xcb-util-image-devel \
    xcb-util-keysyms-devel \
    xcb-util-renderutil-devel \
    xcb-util-wm-devel \
    libicu \
    jq \
    && yum clean all
COPY bin/* /usr/sbin/
RUN systemd-machine-id-setup
