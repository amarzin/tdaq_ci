#!/bin/sh
#
# This script assumes that it will have access to a GIT trigger token
# in $TDAQ_CI_TRIGGER.
#
# It expects a list of releases to test in $releases. 
# 

if [ -f $CI_PROJECT_DIR/.done ]; then
   exit 0
else
   touch $CI_PROJECT_DIR/.done
fi

echo "This way of triggering an automated package build is no longer supported"
echo "Remove your .gitlab-ci.yml file if you don't have any local changes"
echo "or replace the content simply with:"
echo
echo "include: 'https://gitlab.cern.ch/atlas-tdaq-software/tdaq_ci/raw/master/includes/tdaq-ci.yml'"
echo
exit 1

